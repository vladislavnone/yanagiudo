'use strict';

/**
 * Define Themes
 *
 * area: frontend,
 * name: autoSound/default,
 * locale: ru_RU,
 * files: [
 * 'css/styles-m',
 * 'css/styles-l'
 * ],
 * dsl: less
 *
 */

module.exports = {
    activelife: {
        area: 'frontend',
        name: 'ActiveLife/simple',
        locale: 'en_US',
        files: [
            'css/styles-m',
            'css/styles-l',
            'css/email',
            'css/email-inline'
        ],
        dsl: 'less'
    }
};
