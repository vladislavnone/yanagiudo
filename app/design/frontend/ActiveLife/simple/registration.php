<?php
/**
 * Active Life default.
 * @author Vladislav Romaniuk vlad96r@protonmial.com
 * @copyright 2020 Vlromaniuk
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::THEME, 'frontend/ActiveLife/simple', __DIR__);
