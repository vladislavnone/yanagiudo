var config = {
    map: {
        "*": {
            related_carousel: 'ActiveLife_Gallery/js/activelife_carousel',
            owlCarousel: 'ActiveLife_Gallery/js/owl.carousel',
        },
    },
};
