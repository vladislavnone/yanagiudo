<?php
/**
Active Life Gallery registration.
@author Vladislav Romaniuk vlad96r@protonmial.com
@copyright 2020 Vlromaniuk
 */
?>
<?php

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'ActiveLife_Gallery',
    __DIR__
);
